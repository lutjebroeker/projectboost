﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour {

    [SerializeField] float thrustSpeed = 100f;
    [SerializeField] float rotateSpeed = 100f;

    [SerializeField] float timeDying = 3f;
    [SerializeField] float timeTranscending = 3f;

    Rigidbody rigidBody;
    AudioSource audioSource;
    Vector3 startPosition;

    enum State
    {
        alive,
        dying,
        transcending
    }
    State state = State.alive;

	// Use this for initialization
	void Start () {
        rigidBody = gameObject.GetComponent<Rigidbody>();
        audioSource = gameObject.GetComponent<AudioSource>();
        startPosition = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        if(state == State.alive)
            ProcessInput();
	}

    private void ProcessInput()
    {
        ProcessThruster();
        Rotate();
        if (Input.GetKeyUp(KeyCode.R))
            ResetGame();
    }
    
    private void ProcessThruster()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            audioSource.Play();
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            audioSource.Stop();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            rigidBody.AddRelativeForce(Vector3.up * thrustSpeed);
        }
    }
    private void Rotate()
    {
        float rotationThisFrame = rotateSpeed * Time.deltaTime;

        //Handmatig rataion aanpaasen
        rigidBody.freezeRotation = true;
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * rotationThisFrame);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.back * rotationThisFrame);
        }
        rigidBody.freezeRotation = false;
    }
    private void LoadNextSceneOrFinishScreen()
    {
        var currentScene = SceneManager.GetActiveScene();
        if (!IsCurrentSceneTheFinalScene(SceneManager.GetActiveScene()))
        {
            SceneManager.LoadScene(currentScene.buildIndex + 1);
        }
        else
        {
            //We willen uitendelijk natuurlijk een winscherm, maar nu het laatste level nog een keer
            SceneManager.LoadScene(currentScene.buildIndex);
        }
    }

    private bool IsCurrentSceneTheFinalScene(Scene scene)
    {
        bool isFinal = false;

        int sceneCount = SceneManager.sceneCount;

        if (scene.buildIndex == sceneCount) //Het moet -1 omdat buildindex op 0 begint
            isFinal = true;

        return isFinal;
    }

    private void ResetGame()
    {
        //gameObject.transform.rotation = new Quaternion(0,0,0,0);
        //gameObject.transform.position = startPosition;
        //rigidBody.velocity = Vector3.zero;
        //rigidBody.angularVelocity = Vector3.zero;    

        SceneManager.LoadScene(0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (state != State.alive)
            return;

        switch (collision.gameObject.tag)
        {
            case "Friendly":
                break;
            case "Finish":
                state = State.transcending;
                Invoke("LoadNextSceneOrFinishScreen", timeTranscending);
                break;
            default:
                state = State.dying;
                Invoke("ResetGame", timeDying);
                break;

        }
    }
}
